# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(li):
    li1 = []
    li2 = []
    li1_length = len(li)//2 + (len(li)%2)
    for i in range(li1_length):
        li1.append(li[i])
    for i in range(len(li)//2):
        index = i+li1_length
        li2.append(li[index])
    return li1, li2