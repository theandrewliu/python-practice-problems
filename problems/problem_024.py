# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if len(values) == 0:
        return None# If there are no items in the list of values
        # return None
    sum = 0# sum = 0
    for item in values:
        sum+=item# for each item in the list of values
        # add it to the sum
    return sum/len(values)# return the sum divided by the number of items
    # in the list
    pass
