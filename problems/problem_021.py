# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"
#
# There is pseudocode to guide you.

def gear_for_day(is_workday, is_sunny):
    # Create an empty list that will hold the different gear
    # gear = new empty list
    gear = []
    if is_workday == True and is_sunny == False:# If it is a workday and it is not sunny
        gear.append("umbrella") # Add "umbrella" to gear
        # gear.append("umbrella")
    if is_workday == True: # If it is a workday
        gear.append("laptop")# Add "laptop" to gear
    else:# Otherwise
        gear.append("surfboard")# Add "surfboard" to gear
    return gear# Return the list of gear
