# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three
# items.
#
# There is pseudocode to guide you.


def can_make_pasta(ingredients):
    if "flour" in ingredients and "eggs" in ingredients and "oil" in ingredients:
        return True
    else:
        return False
    # If "flour" is in ingredients and "eggs" is
    # in ingredients and "oil" is in ingredients
        # return True
    # Otherwise
        # return False
    pass
