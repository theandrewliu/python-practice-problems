# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lowcount = 0
    upcount = 0
    digitcount = 0
    specialcount = 0
    if len(password) < 6 or len(password) > 12:
        return False
    for letter in password:
        if letter.isalpha() and letter.islower():
            lowcount+=1
        elif letter.isalpha() and letter.isupper():
            upcount+=1
        elif letter.isdigit():
            digitcount+=1
        elif letter =="$" or letter =="!" or letter=="@":
            specialcount+=1
    if lowcount > 0 and upcount > 0 and digitcount > 0 and specialcount > 0:
        return True
    else:
        return False


    pass
